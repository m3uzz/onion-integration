<?php
/**
 * This file is part of Onion
 *
 * Copyright (c) 2014-2016, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionInt
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2016 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-integration
 */

namespace OnionInt\Asterisk\Manager;
use OnionLib\Formate;

class AsteriskManager
{

	protected $rSocket = null;

	protected $nResponseTimeOut = 1;

	protected $nResponseTimeOutMicro = 0;

	protected $bProxyConnection = true;

	protected $aResponse = array();

	protected $aError;

	protected $aService = array();

	protected $aParams = array();

	protected $sResoponseType = 'http'; // json, stream

	protected $sActionId = "Onion";

	protected $sAudioPath = "/data\/uploads\/audio/";

	protected $sAudioPathReal = "datastorage/audios";
	

	/**
	 */
	public function __construct ()
	{
		if ($_SERVER['HTTP_ACCEPT'] == "text/event-stream")
		{
			$this->sResoponseType = 'stream';
		}
		elseif ($_SERVER['HTTP_ACCEPT'] == "application/json")
		{
			$this->sResoponseType = 'json';
		}
	}
	

	/**
	 *
	 * @param string $psVar
	 * @param mixed $pmValue
	 * @return \OnionInt\Asterisk\AsteriskManager
	 */
	public function set ($psVar, $pmValue)
	{
		if (property_exists($this, $psVar))
		{
			$this->$psVar= $pmValue;
		}
		
		return $this;
	}

	
	/**
	 *
	 * @param string $psVar
	 */
	public function get ($psVar)
	{
		if (property_exists($this, $psVar))
		{
			return $this->$psVar;
		}
	}

	
	/**
	 */
	public function getParams ()
	{
		if (isset($_SERVER['argv']) && is_array($_SERVER['argv']))
		{
			foreach ($_SERVER['argv'] as $lsArg)
			{
				$laArg = explode("=", $lsArg);
				
				if (isset($laArg[1]))
				{
					$this->aParams[$laArg[0]] = $laArg[1];
				}
			}
		}
		elseif (isset($_GET) && is_array($_GET))
		{
			$this->aParams = $_GET;
		}
	}

	
	/**
	 *
	 * @param array $paService
	 */
	public function setService ($paService)
	{
		$this->aService = array(
			'host' => $paService['host'],
			'port' => $paService['port'],
			'user' => $paService['user'],
			'pass' => $paService['pass'],
			'context' => $paService['context']
		);
		
		$this->set('bProxyConnection', $paService['proxy']);
	}

	
	/**
	 *
	 * @param string $psVar
	 * @param string $psValue
	 */
	public function setParam ($psVar, $psValue)
	{
		$this->aParams[$psVar] = $psValue;
	}

	
	/**
	 *
	 * @param boolean $pbJson
	 * @return string|array
	 */
	public function getLog ($pbJson = false)
	{
		if ($pbJson)
		{
			return json_encode($this->aResponse);
		}
		
		return $this->aResponse;
	}

	
	/**
	 *
	 * @param string $psVar
	 * @param string $psDefault
	 * @return multitype
	 */
	public function request ($psVar, $psDefault = null)
	{
		if (isset($this->aParams[$psVar]))
		{
			return $this->aParams[$psVar];
		}
		else
		{
			return $psDefault;
		}
	}

	
	/**
	 *
	 * @return string
	 */
	public function response ()
	{
		if ($this->sResoponseType == 'json')
		{
			header("Content-Type: application/json");
			return json_encode($this->aResponse);
		}
		elseif ($this->sResoponseType == 'stream')
		{
			header("Content-type: text/event-stream");
			$lsResponse = json_encode($this->aResponse);
			return "data: " . $lsResponse . "\n\n";
		}
		else
		{
			//Debug::display($this->aResponse);
		}
	}

	
	/**
	 *
	 * @param string $psAct
	 * @return boolean
	 */
	public function manager ($psAct = '')
	{
		$lsAct = $this->request('act', $psAct);
		$lsNextAction = $this->request('nextaction', '');
		$lnId = $this->request('id', '');
		$lnExtension = $this->request('extension', '');
		$lnPhone = $this->request('phone', '');
		$lsSip = $this->request('sip', '');
		$lsAudioChannel = $this->request('audiochannel', '');
		$lsAudioPath = $this->request('audio', '');
		
		$lsAudio = preg_replace($this->sAudioPath, $this->sAudioPathReal, $lsAudioPath);
		$lnExtension = Formate::formatPhone($lnExtension);
		$lnPhone = Formate::formatPhone($lnPhone);
		
		$this->set('sActionId', $this->get('sActionId') . "-{$lnId}");
		
		$this->aResponse[0]['Act'] = $lsAct;
		$this->aResponse[0]['NextAction'] = $lsNextAction;
		$this->aResponse[0]['Id'] = $lnId;
		$this->aResponse[0]['Extension'] = $lnExtension;
		$this->aResponse[0]['Phone'] = $lnPhone;
		$this->aResponse[0]['Audio'] = $lsAudio;
		$this->aResponse[0]['Sip'] = $lsSip;
		$this->aResponse[0]['AudioChannel'] = $lsAudioChannel;
		$this->aResponse[0]['Play'] = false;
		
		switch ($lsAct)
		{
			case "call":
				$lbReturn = $this->call($lnExtension, $lnPhone);
			break;
			case "hangup":
				$lbReturn = $this->hangup($lsSip);
			break;
			case "play":
				$lbReturn = $this->playAudio($lsSip, $lsAudio);
			break;
			case "stop":
				$lbReturn = $this->stopAudio($lsAudioChannel);
			break;
			case "queue":
				$lbReturn = $this->queue($lnExtension);
			break;
			case "status":
				$this->nResponseTimeOut = 1;
				$lbReturn = $this->status($lnId, $lnExtension, $lnPhone, $lsAudioChannel);
			break;
		}
		
		if (! isset($this->aResponse[0]['Response']))
		{
			$this->aResponse[0]['Response'] = 'Error';
		}
		
		$this->aResponse[0]['Return'] = $lbReturn;
		$this->aResponse[0]['ErrorMessage'] = $this->aError;
		
		return $lbReturn;
	}

	
	/**
	 *
	 * @param string $psCommandLine
	 * @param boolean $pbClose
	 */
	public function sendCommand ($psCommandLine, $pbClose = false)
	{
		$lsEndLine = "\r\n";
		
		if ($pbClose)
		{
			$lsEndLine .= "\r\n";
		}
		
		fputs($this->rSocket, "{$psCommandLine}{$lsEndLine}");
	}

	
	/**
	 *
	 * @return boolean
	 */
	public function getResponse ()
	{
		stream_set_timeout($this->rSocket, $this->nResponseTimeOut, $this->nResponseTimeOutMicro);
		$lsLine = true;
		$lnCount = 0;
		
		$this->aResponse[0]['Event'] = 'Head';
		
		while (! feof($this->rSocket) && $lsLine !== false)
		{
			$lsLine = stream_get_line($this->rSocket, 1024, "\n");
			
			if ($lsLine !== false)
			{
				$laLine = explode(":", trim($lsLine));
				
				if (isset($laLine[1]))
				{
					if ($laLine[0] == 'Event')
					{
						$lnCount ++;
					}
					
					$this->aResponse[$lnCount][$laLine[0]] = trim($laLine[1]);
				}
			}
		}
		
		if (isset($this->aResponse[0]['Response']) && $this->aResponse[0]['Response'] == 'Success')
		{
			return true;
		}
		
		return false;
	}
	

	/**
	 *
	 * @return boolean
	 */
	public function connect ($psEvent = 'off')
	{
		$this->rSocket = @fsockopen($this->aService['host'], $this->aService['port'], $lnErrno, $lsErrstr, 10);
		
		if ($this->rSocket)
		{
			if ($this->bProxyConnection)
			{
				return true;
			}
			else
			{
				$this->sendCommand("Action: Login");
				$this->sendCommand("UserName: {$this->aService['user']}");
				$this->sendCommand("Secret: {$this->aService['pass']}");
				$this->sendCommand("Events: {$psEvent}", true);
				
				if ($this->getResponse())
				{
					return true;
				}
				else
				{
					$this->aError[] = "Asterisk Manager auth error!";
				}
			}
		}
		else
		{
			$this->aError[] = "Asterisk Manager connect error!";
		}
		
		return false;
	}

	
	/**
	 *
	 * @param int $pnExtension
	 * @param int $pnPhone
	 * @return boolean
	 */
	public function call ($pnExtension, $pnPhone)
	{
		// Verifica se já existe uma chamada em damento para o ramal informado
		// Se sim ele retorna true e recupera o controle da chamada em curso.
		// Senão tenta iniciar uma nova chamada.
		if ($this->queue($pnExtension))
		{
			return true;
		}
		elseif (! empty($pnExtension) && ! empty($pnPhone))
		{
			if ($this->connect('call'))
			{
				$lsChannel = "Sip/{$pnExtension}_{$this->aService['context']}";
				
				$this->sendCommand("Action: Originate");
				$this->sendCommand("Channel:{$lsChannel}");
				$this->sendCommand("Exten: {$pnPhone}");
				$this->sendCommand("Context: {$this->aService['context']}-from-internal");
				$this->sendCommand("Priority: 1");
				$this->sendCommand("ActionId: {$this->sActionId}-Call");
				$this->sendCommand("Async: yes", true);
				
				if ($this->getResponse())
				{
					$lbReturn = false;
					
					foreach ($this->aResponse as $laEvent)
					{
						if ($laEvent['Event'] == 'OriginateResponse' && $laEvent['Channel'] == $lsChannel && $laEvent['Exten'] == $pnPhone && $laEvent['Response'] == 'Failure')
						{
							$this->aResponse[0]['Response'] = $laEvent['Response'];
							
							switch ($laEvent['Reason'])
							{
								case 0:
									$this->aError[] = "Extension {$pnExtension} not registred!";
								break;
								case 1:
									$this->aError[] = "Local extension {$pnExtension} no answer!";
								break;
								case 2:
									$this->aError[] = "Ringing local extension {$pnExtension}!";
								break;
								case 3:
									$this->aError[] = "Ringing remote phone {$pnPhone}!";
								break;
								case 4:
									$this->aError[] = "Remote phone {$pnPhone} answered!";
								break;
								case 5:
									$this->aError[] = "Remote phone {$pnPhone} busy!";
								break;
								case 6:
									$this->aError[] = "Make it go off hook!";
								break;
								case 7:
									$this->aError[] = "Line is off hook!";
								break;
								case 8:
									$this->aError[] = "Congestion!";
								break;
							}
							
							$lbReturn = false;
						}
						elseif ($laEvent['Event'] == 'Newchannel' && $laEvent['CallerIDNum'] == $pnExtension)
						{
							$this->aResponse[0]['Sip'] = $laEvent['Channel'];
							
							$lbReturn = true;
						}
					}
					
					return $lbReturn;
				}
			}
		}
		else
		{
			$this->aError[] = "Extension or phone undefined!";
		}
		
		return false;
	}

	
	/**
	 *
	 * @return boolean
	 */
	public function queue ($pnExtension)
	{
		if (! empty($pnExtension))
		{
			if ($this->connect('call'))
			{
				$this->sendCommand("Action: CoreShowChannels");
				$this->sendCommand("ActionId: {$this->sActionId}-Call", true);
				
				if ($this->getResponse())
				{
					$lsChannel = "/^SIP\/{$pnExtension}_{$this->aService['context']}-.*$/";
					$lbReturn = false;
					
					foreach ($this->aResponse as $laEvent)
					{
						if ($laEvent['Event'] == 'CoreShowChannel' && preg_match($lsChannel, $laEvent['Channel']))
						{
							if (preg_match('/^SIP\/.*-peer\/(.*?),[0-9]{3}/', $laEvent['ApplicationData'], $laPhone))
							{
								$this->aResponse[0]['Phone'] = $laPhone[1];
							}
							
							$this->aResponse[0]['Sip'] = $laEvent['Channel'];
						
							$lbReturn = true;
						}
					}
					
					if (!$lbReturn)
					{
						$this->aResponse[0]['Response'] = "Error";
						$this->aError[] = "There is no channel for this extension!";
					}
					
					return $lbReturn;
				}
			}
		}
		else
		{
			$this->aError[] = "Extension undefined!";
		}
		
		return false;
	}

	
	/**
	 *
	 * @param int $pnId
	 * @param int $pnExtension
	 * @param int $pnPhone
	 * @return boolean
	 */
	public function status ($pnId, $pnExtension, $pnPhone, $psAudioChannel)
	{
		if (! empty($pnId) && ! empty($pnExtension) && ! empty($pnPhone))
		{
			if ($this->connect('call'))
			{
				$this->sendCommand("Action: CoreShowChannels");
				$this->sendCommand("ActionId: {$this->sActionId}-Status", true);
				
				if ($this->getResponse())
				{
					$lbReturn = false;
					
					foreach ($this->aResponse as $laEvent)
					{
						if ($laEvent['Event'] == 'CoreShowChannel' && $laEvent['CallerIDnum'] == $pnExtension && $laEvent['ChannelState'] == '5')
						{
							$this->aResponse[0]['Sip'] = $laEvent['Channel'];
							$this->aResponse[0]['Act'] = 'ringingExtension';
							$lbReturn = true;
						}
						elseif ($laEvent['Event'] == 'CoreShowChannel' && $laEvent['CallerIDnum'] == $pnExtension && $laEvent['ChannelState'] == '0')
						{
							$this->aResponse[0]['Sip'] = $laEvent['Channel'];
							$this->aResponse[0]['Act'] = 'extensionAnswer';
							$lbReturn = true;
						}
						elseif ($laEvent['Event'] == 'CoreShowChannel' && $laEvent['CallerIDnum'] == $pnPhone && $laEvent['ChannelState'] == '5')
						{
							$this->aResponse[0]['Act'] = 'ringingPhone';
							$lbReturn = true;
						}
						elseif ($laEvent['Event'] == 'CoreShowChannel' && $laEvent['CallerIDnum'] == $pnPhone && $laEvent['ChannelState'] == '0')
						{
							$this->aResponse[0]['Act'] = 'bridge';
							$lbReturn = true;
						}
						elseif ($laEvent['Event'] == 'CoreShowChannel' && $laEvent['CallerIDnum'] == $pnPhone && $laEvent['ChannelState'] == '6')
						{
							$this->aResponse[0]['Act'] = 'phoneAnswer';
							$lbReturn = true;
						}
						elseif ($laEvent['Event'] == 'CoreShowChannel' && $laEvent['Application'] == 'MP3Player' && $laEvent['ChannelState'] == '6')
						{
							$this->aResponse[0]['Play'] = true;
							$lbReturn = true;
						}
					}
					
					if (! $lbReturn)
					{
						$this->aResponse[0]['Act'] = 'hangup';
					}
					
					return $lbReturn;
				}
			}
		}
		else
		{
			$this->aError[] = "Id, extension or phone undefined!";
		}
		
		return false;
	}

	
	/**
	 *
	 * @param string $psSip
	 * @return boolean
	 */
	public function hangup ($psSip)
	{
		if (! empty($psSip))
		{
			if ($this->connect())
			{
				$this->sendCommand("Action: Hangup");
				$this->sendCommand("ActionId: {$this->sActionId}-HangUp");
				$this->sendCommand("Channel: {$psSip}", true);
				
				if ($this->getResponse())
				{
					return true;
				}
				elseif ($this->aResponse[0]['Response'] == "Error" && $this->aResponse[0]['Message'] == "No such channel")
				{
					return true;
				}
			}
		}
		else
		{
			$this->aError[] = "Undefined Sip channel to hangup!";
		}
		
		return false;
	}

	
	/**
	 *
	 * @param string $psSip
	 * @param string $psAudio
	 * @return boolean
	 */
	public function playAudio ($psSip, $psAudio)
	{
		if (! empty($psSip) && ! empty($psAudio))
		{
			if ($this->connect('call'))
			{
				$this->sendCommand("Action: Originate");
				$this->sendCommand("ActionId: {$this->sActionId}-PlayAudio");
				$this->sendCommand("Channel: Local/123@{$this->aService['context']}-telemessage");
				$this->sendCommand("Exten: s");
				$this->sendCommand("Context: {$this->aService['context']}-telemessage");
				$this->sendCommand("Priority: 1");
				$this->sendCommand("Variable: chanvar={$psSip}");
				$this->sendCommand("Variable: audiovar={$psAudio}");
				$this->sendCommand("Async: yes", true);
				
				if ($this->getResponse())
				{
					$lbReturn = false;
					
					foreach ($this->aResponse as $laEvent)
					{
						if ($laEvent['Event'] == 'LocalBridge')
						{
							$this->aResponse[0]['AudioChannel'] = $laEvent['Channel1'];
							$this->aResponse[0]['Play'] = true;
							
							$lbReturn = true;
						}
					}
					
					if (! $lbReturn)
					{
						$this->aResponse[0]['Act'] = 'stop';
						$this->aResponse[0]['Play'] = false;
					}
					
					return $lbReturn;
				}
			}
		}
		elseif (empty($psSip))
		{
			$this->aError[] = "Undefined Sip channel to play!";
		}
		elseif (empty($psAudio))
		{
			$this->aError[] = "Undefined audio path to play!";
		}
		
		return false;
	}

	
	/**
	 *
	 * @param string $psAudioChannel
	 * @return boolean
	 */
	public function stopAudio ($psAudioChannel)
	{
		if (! empty($psAudioChannel))
		{
			if ($this->connect())
			{
				$this->sendCommand("Action: Hangup");
				$this->sendCommand("ActionId: {$this->sActionId}-StopAudio");
				$this->sendCommand("Channel: {$psAudioChannel}", true);
				
				if ($this->getResponse())
				{
					$this->aResponse[0]['Play'] = false;
					$this->aResponse[0]['AudioChannel'] = '';
					
					return true;
				}
			}
		}
		else
		{
			$this->aError[] = "Undefined audio channel to stop play!";
		}
		
		return false;
	}
}