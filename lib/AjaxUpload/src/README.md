######################################### 
# Implementação no add form
######################################### 

    <div id="uploadArea" class="form element" style="margin:30px; width:57%;">
        <div id="statusBox" style="margin-top:20px;"></div>
        <div id="actionBox" style="margin-top:20px;">
            <button id="btnFileUpload" class="btn btn-inverse btn-large"><i class="icon-upload icon-white"></i> Selecione um arquivo</button>
        </div>
    </div>
    
    <script type="text/javascript" src="/library/simpleUpload/SimpleAjaxUploader.min.js"></script>  
    <script type="text/javascript">
        var idPrefix = 'componentes-file-files-add-';
        var btn = document.getElementById(idPrefix + 'btnFileUpload'); 
        var statusBox = document.getElementById(idPrefix + 'statusBox'); // container for file size info
        var actionBox = document.getElementById(idPrefix + 'actionBox'); // the element we're using for a progress bar
        
        var uploader = new ss.SimpleUpload({
          button: btn, // file upload button
          url: 'backend/file/add', // server side handler
          name: 'uploadFile', // upload parameter name        
          progressUrl: 'backend/file/add?progress', // enables cross-browser progress support (more info below)
          responseType: 'json',
          multiple: false,
          maxUploads: 1,
          allowedExtensions: ['pdf','doc','docx','odt','xls','xlsx','ods','ppt','pptx','odp','txt','csv','xml','jpg','png','gif'],
          maxSize: 5120, // kilobytes
          hoverClass: 'ui-state-hover',
          focusClass: 'ui-state-focus',
          disabledClass: 'ui-state-disabled',

          onExtError: function(filename, extension) {
              statusBox.innerHTML = '<div class="alert alert-error"> <button type="button" class="close" data-dismiss="alert">&times;</button><h4>Erro!</h4>' + filename + ' não é um tipo de arquivo permitido.</div>';
          },
                
          onSizeError: function(filename, fileSize) {
              statusBox.innerHTML = '<div class="alert alert-error"> <button type="button" class="close" data-dismiss="alert">&times;</button><h4>Erro!</h4>' + filename + ' é muito grande. (5120kb é o tamanho máximo) </div>';
          },
          
          onSubmit: function(filename, extension){
            // Create the elements of our progress bar
              var progress = document.createElement('div'), // container for progress bar
                  bar = document.createElement('div'), // actual progress bar
                  fileSize = document.createElement('div'), // container for upload file size
                  wrapper = document.createElement('div'); // container for this progress bar
              
              // Assign each element its corresponding class
              progress.className = 'progress progress-striped active';
              bar.className = 'bar';            
              fileSize.className = 'size';
              wrapper.className = 'wrapper';
              
              // Assemble the progress bar and add it to the page
              progress.appendChild(bar); 
              wrapper.innerHTML = '<div class="name">'+filename+'</div>'; // filename is passed to onSubmit()
              wrapper.appendChild(fileSize);
              wrapper.appendChild(progress);                                       
              actionBox.replaceChild(wrapper, btn); // just an element on the page to hold the progress bars    
              
              // Assign roles to the elements of the progress bar
              this.setProgressBar(bar); // will serve as the actual progress bar
              this.setFileSizeBox(fileSize); // display file size beside progress bar
              this.setProgressContainer(wrapper); // designate the containing div to be removed after upload
          },
          
          onComplete: function(filename, response){
              if (!response) {
                  statusBox.innerHTML = '<div class="alert alert-error"> <button type="button" class="close" data-dismiss="alert">&times;</button><h4>Erro!</h4>Desculpe, ocorreu um erro ao tentar enviar o arquivo ' + filename + '. Tente novamente mais tarde.</div>';
                  actionBox.appendChild(btn);
                  return false;            
              }
              else if (response.success === false){
                  statusBox.innerHTML = '<div class="alert alert-error"> <button type="button" class="close" data-dismiss="alert">&times;</button><h4>Erro!</h4>' + filename + ' ' + response.msg + '</div>';
                  actionBox.appendChild(btn);
                  return false;
              }           
              else if (response.success === true){
                  actionBox.innerHTML = '<a href=" ' + response.url + ' " target="_blank" class="btn btn-large btn-primary"><i class="icon-file icon-white"></i> ' + filename + '</a>';
                  actionBox.innerHTML += '<button onclick="btnAlterar();" class="btn btn-link btn-small"><i class="icon-repeat"></i> Alterar</button>';
                  statusBox.innerHTML = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">&times;</button>O arquivo <span class="label label-success">' + filename + '</span> foi enviado com sucesso.</div>';

                  document.getElementById(idPrefix + 'stTitle').value = response.title;
                  document.getElementById(idPrefix + 'stPath').value = response.file;
                  document.getElementById(idPrefix + 'stType').value = response.ext;
                  document.getElementById(idPrefix + 'stVersion').value = '1';
              }
          }
        });

        function btnAlterar()
        {
            statusBox.innerHTML = '';
            actionBox.innerHTML = '';
            actionBox.appendChild(btn);             

            document.getElementById(idPrefix + 'stTitle').value = '';
            document.getElementById(idPrefix + 'stPath').value = '';
            document.getElementById(idPrefix + 'stType').value = '';
            document.getElementById(idPrefix + 'stVersion').value = '1';
        }
    </script>


######################################### 
# Implementação no edit form
#########################################

    <div id="uploadArea" class="form element" style="margin:30px; width:57%;">
        <div id="statusBox" style="margin-top:20px;"></div>
        <div id="actionBox" style="margin-top:20px;">
            <button id="btnFileUpload" class="btn btn-inverse btn-large" style="display:none"><i class="icon-upload icon-white"></i> Selecione um arquivo</button>
            <a href="<?php echo $gsUrl; ?>" target="_blank" class="btn btn-large btn-primary"><i class="icon-file icon-white"></i> <?php echo $gsFileName; ?></a>
            <button onclick="btnAlterar();" class="btn btn-link btn-small"><i class="icon-repeat"></i> Alterar</button>
        </div>
    </div>
    
    <script type="text/javascript" src="/library/simpleUpload/SimpleAjaxUploader.min.js"></script>  
    <script type="text/javascript">
        var fileName = '<?php echo $gsFileName; ?>';
        var idPrefix = 'componentes-file-files-edit-id-<?php echo $gsId; ?>-';
        var btn = document.getElementById(idPrefix + 'btnFileUpload'); 
        var statusBox = document.getElementById(idPrefix + 'statusBox'); // container for file size info
        var actionBox = document.getElementById(idPrefix + 'actionBox'); // the element we're using for a progress bar
        
        var uploader = new ss.SimpleUpload({
          button: btn, // file upload button
          url: 'backend/file/edit', // server side handler
          name: 'uploadFile', // upload parameter name        
          progressUrl: 'backend/file/edit?progress', // enables cross-browser progress support (more info below)
          responseType: 'json',
          multiple: false,
          maxUploads: 1,
          allowedExtensions: ['pdf','doc','docx','odt','xls','xlsx','ods','ppt','pptx','odp','txt','csv','xml','jpg','png','gif'],
          maxSize: 5120, // kilobytes
          hoverClass: 'ui-state-hover',
          focusClass: 'ui-state-focus',
          disabledClass: 'ui-state-disabled',

          onExtError: function(filename, extension) {
              statusBox.innerHTML = '<div class="alert alert-error"> <button type="button" class="close" data-dismiss="alert">&times;</button><h4>Erro!</h4>' + filename + ' não é um tipo de arquivo permitido.</div>';
          },
                
          onSizeError: function(filename, fileSize) {
              statusBox.innerHTML = '<div class="alert alert-error"> <button type="button" class="close" data-dismiss="alert">&times;</button><h4>Erro!</h4>' + filename + ' é muito grande. (5120kb é o tamanho máximo) </div>';
          },
          
          onSubmit: function(filename, extension){
            // Create the elements of our progress bar
              var progress = document.createElement('div'), // container for progress bar
                  bar = document.createElement('div'), // actual progress bar
                  fileSize = document.createElement('div'), // container for upload file size
                  wrapper = document.createElement('div'); // container for this progress bar
              
              // Assign each element its corresponding class
              progress.className = 'progress progress-striped active';
              bar.className = 'bar';            
              fileSize.className = 'size';
              wrapper.className = 'wrapper';
              
              // Assemble the progress bar and add it to the page
              progress.appendChild(bar); 
              wrapper.innerHTML = '<div class="name">'+filename+'</div>'; // filename is passed to onSubmit()
              wrapper.appendChild(fileSize);
              wrapper.appendChild(progress);                                       
              actionBox.replaceChild(wrapper, btn); // just an element on the page to hold the progress bars    
              
              // Assign roles to the elements of the progress bar
              this.setProgressBar(bar); // will serve as the actual progress bar
              this.setFileSizeBox(fileSize); // display file size beside progress bar
              this.setProgressContainer(wrapper); // designate the containing div to be removed after upload
          },
          
          onComplete: function(filename, response){
              if (!response) {
                  statusBox.innerHTML = '<div class="alert alert-error"> <button type="button" class="close" data-dismiss="alert">&times;</button><h4>Erro!</h4>Desculpe, ocorreu um erro ao tentar enviar o arquivo ' + filename + '. Tente novamente mais tarde.</div>';
                  actionBox.appendChild(btn);
                  return false;            
              }
              else if (response.success === false){
                  statusBox.innerHTML = '<div class="alert alert-error"> <button type="button" class="close" data-dismiss="alert">&times;</button><h4>Erro!</h4>' + filename + ' ' + response.msg + '</div>';
                  actionBox.appendChild(btn);
                  return false;
              }           
              else if (response.success === true){
                  actionBox.innerHTML = '<a href=" ' + response.url + ' " target="_blank" class="btn btn-large btn-primary"><i class="icon-file icon-white"></i> ' + filename + '</a>';
                  actionBox.innerHTML += '<button onclick="btnAlterar();" class="btn btn-link btn-small"><i class="icon-repeat"></i> Alterar</button>';
                  statusBox.innerHTML = '<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">&times;</button>O arquivo <span class="label label-success">' + filename + '</span> foi enviado com sucesso.</div>';

                  document.getElementById(idPrefix + 'stTitle').value = response.title;
                  document.getElementById(idPrefix + 'stPath').value = response.file + '#CHANGED#' + fileName;
                  document.getElementById(idPrefix + 'stType').value = response.ext;
              }
          }
        });

        function btnAlterar()
        {
            statusBox.innerHTML = '';
            actionBox.innerHTML = '';
            actionBox.appendChild(btn);
            btn.style = "display:block;";

            document.getElementById(idPrefix + 'stPath').value = '';
            document.getElementById(idPrefix + 'stType').value = '';
        }
    </script>