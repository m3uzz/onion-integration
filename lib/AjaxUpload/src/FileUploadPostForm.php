<?php
/**
 * Simple Ajax Uploader
 * Version 1.8.2
 * https://github.com/LPology/Simple-Ajax-Uploader
 *
 * Copyright 2013 LPology, LLC
 * Released under the MIT license
 *
 * View the documentation for an example of how to use this class.
 */

namespace AjaxUpload;

/**
* Handles form uploads through hidden iframe
* Used by FileUpload below -- don't call this class directly.
*/
final class FileUploadPOSTForm 
{
  	public $sUploadName;
  	
  	
  	/**
  	 * 
  	 * @param string $psSavePath
  	 * @return bool
  	 */
  	final public function Save($psSavePath) 
  	{
    	if (move_uploaded_file($_FILES[$this->sUploadName]['tmp_name'], $psSavePath)) 
    	{
      		return true;
    	}
    	
    	return false;
  	}
  
  	
  	/**
  	 * 
  	 * @return string
  	 */
  	final public function getFileName() 
  	{
    	return $_FILES[$this->sUploadName]['name'];
  	}
  
  	
  	/**
  	 * 
  	 * @return int
  	 */
  	final public function getFileSize() 
  	{
    	return $_FILES[$this->sUploadName]['size'];
  	}
}