<?php
/**
 * Simple Ajax Uploader
 * Version 1.8.2
 * https://github.com/LPology/Simple-Ajax-Uploader
 *
 * Copyright 2013 LPology, LLC
 * Released under the MIT license
 *
 * View the documentation for an example of how to use this class.
 */

namespace AjaxUpload;
use AjaxUpload\FileUploadPOSTForm;
use AjaxUpload\FileUploadXHR;
use OnionLib\System;


/**
* Main class for handling file uploads
*/
class SimpleUploader
{
  	protected $sUploadDir;                    // File upload directory (include trailing slash)
  	
  	protected $aAllowedExtensions;            // Array of permitted file extensions
  	
  	protected $nSizeLimit = 10485760;         // Max file upload size in bytes (default 10MB)
  	
  	protected $sNewFileName;                  // Optionally save uploaded files with a new name by setting this
  	
  	protected $sTitle;						// Original file name
  	
  	protected $sFileName;                    // Filename of the uploaded file
  	
  	protected $nFileSize;                    // Size of uploaded file in bytes
  	
  	protected $sFileExtension;               // File extension of uploaded file
  	
  	protected $sSavedFile;                   // Path to newly uploaded file (after upload completed)
  	
  	protected $sErrorMsg;                    // Error message if handleUpload() returns false (use getErrorMsg() to retrieve)
  	
  	protected $oHandler;

  	
	/**
	 * Magic setter to save protected properties.
	 *
	 * @param string $psProperty
	 * @param mixed $pmValue
	 */
	public function __set ($psProperty, $pmValue)
	{
		$this->set($psProperty, $pmValue);
	}
	
	
	/**
	 * 
	 * @param string $psProperty
	 * @param mixed $pmValue
	 */
	public function set ($psProperty, $pmValue)
	{
		if (property_exists($this, $psProperty))
		{
			$this->$psProperty = $pmValue;
		}
	}
	
	
	/**
	 * Magic getter to expose protected properties.
	 *
	 * @param string $psProperty
	 * @return mixed
	 */
	public function __get ($psProperty)
	{
		return $this->get($psProperty);
	}
	
	
	/**
	 * 
	 * @param string $psProperty
	 * @return mixed
	 */
	public function get ($psProperty)
	{
		if (property_exists($this, $psProperty))
		{
			return $this->$psProperty;
		}
	}
	
  	
	/**
	 * 
	 * @param string $psUploadName
	 */
  	public function uploader ($psUploadName) 
  	{
    	if (isset($_FILES[$psUploadName])) 
    	{
      		$this->oHandler = new FileUploadPOSTForm(); // Form-based upload
    	}
    	elseif (isset($_GET[$psUploadName])) 
    	{
      		$this->oHandler = new FileUploadXHR(); // XHR upload
    	} 
    	else 
    	{
      		$this->oHandler = false;
    	}

    	if ($this->oHandler) 
    	{
      		$this->oHandler->sUploadName = $psUploadName;
      		$this->sFileName = $this->oHandler->getFileName();
      		$this->nFileSize = $this->oHandler->getFileSize();
      		
      		$laFileInfo = pathinfo($this->sFileName);
      		
      		if (array_key_exists('extension', $laFileInfo)) 
      		{
        		$this->sFileExtension = strtolower($laFileInfo['extension']);
      		}
    	}
  	}
  	

  	/**
  	 * 
  	 * @return string
  	 */
  	public function getTitle ()
  	{
  		return $this->sTitle;
  	}
  	
  	
  	/**
  	 * 
  	 * @return string
  	 */
  	public function getFileName () 
  	{
    	return $this->sFileName;
  	}
  	

  	/**
  	 * @return int
  	 */
  	public function getFileSize () 
  	{
    	return $this->nFileSize;
  	}

  	
  	/**
  	 * 
  	 * @return string
  	 */
  	public function getExtension () 
  	{
    	return $this->sFileExtension;
  	}
  	

  	/**
  	 * @return string
  	 */
  	public function getErrorMsg () 
  	{
    	return $this->sErrorMsg;
  	}
  	

  	/**
  	 * @return string
  	 */
  	public function getSavedFile () 
  	{
    	return $this->sSavedFile;
  	}

  	
  	/**
  	 * 
  	 * @param string $psExt
  	 * @param array $paAllowedExtensions
  	 * @return bool
  	 */
  	private function checkExtension ($psExt, $paAllowedExtensions) 
  	{
    	if (!is_array($paAllowedExtensions)) 
    	{
      		return false;
    	}
    	if (!in_array(strtolower($psExt), array_map('strtolower', $paAllowedExtensions))) 
    	{
      		return false;
    	}
    	
    	return true;
  	}

  	
  	/**
  	 * 
  	 * @param string $psMsg
  	 */
  	private function setErrorMsg ($psMsg) 
  	{
    	$this->sErrorMsg = $psMsg;
  	}

  	
  	/**
  	 * 
  	 * @param string $psDir
  	 * @return string
  	 */
  	private function fixDir ($psDir) 
  	{
    	$lsLast = substr($psDir, -1);
    	
    	if ($lsLast == '/' || $lsLast == '\\') 
    	{
      		$psDir = substr($psDir, 0, -1);
    	}
    	
    	return $psDir . DS;
  	}

  	
  	/**
  	 * 
  	 * @param string $psUploadDir
  	 * @param array $paAllowedExtensions
  	 * @return bool
  	 */
  	public function handleUpload ($psUploadDir = null, $paAllowedExtensions = null) 
  	{
    	if ($this->oHandler === false) 
    	{
      		$this->setErrorMsg('Incorrect upload name or no file uploaded');
      		return false;
    	}

    	if (!empty($psUploadDir)) 
    	{
      		$this->sUploadDir = $psUploadDir;
    	}
    	
    	if (is_array($paAllowedExtensions)) 
    	{
      		$this->aAllowedExtensions = $paAllowedExtensions;
    	}

    	$this->sUploadDir = $this->fixDir($this->sUploadDir);

    	$this->sTitle = strtolower($this->getFileName());
    	$laTitle = pathinfo($this->sTitle);
    	$this->sTitle = $laTitle['filename'];
    	$this->sTitle = preg_replace(array('/\s\s+/', '/_/'), ' ', $this->sTitle);
    	$this->sTitle = ucfirst($this->sTitle);
    	 
    	if (!empty($this->sNewFileName)) 
    	{
      		$this->sFileName = $this->sNewFileName;
      		$this->sSavedFile = $this->sUploadDir.$this->sNewFileName;
    	}
    	else 
    	{
      		$this->sSavedFile = $this->sUploadDir.$this->sFileName;
    	}

    	if ($this->nFileSize == 0) 
    	{
      		$this->setErrorMsg('File is empty');
      		return false;
    	}
    	
    	if (!is_writable($this->sUploadDir)) 
    	{
      		$this->setErrorMsg('Upload directory is not writable');
      		return false;
    	}
    	
    	if ($this->nFileSize > $this->nSizeLimit) 
    	{
      		$this->setErrorMsg('File size exceeds limit (' . $this->nFileSize . '>' . $this->nSizeLimit .')');
      		return false;
    	}
    	
    	if (!empty($this->aAllowedExtensions)) 
    	{
      		if (!$this->checkExtension($this->sFileExtension, $this->aAllowedExtensions)) 
      		{
        		$this->setErrorMsg('Invalid file type');
        		return false;
      		}
    	}
    	
    	if (!$this->oHandler->Save($this->sSavedFile)) 
    	{
      		$this->setErrorMsg('File could not be saved');
      		return false;
    	}

    	return true;
  	}
  	
  	
  	/**
  	 * 
  	 * @param string $psVarName
  	 * @param string $psValidExtensions
  	 * @param string $psNewFileName
  	 */
  	public function uploadToTemp ($psVarName, $psValidExtensions = null, $psNewFileName = null)
  	{
  		$lsUploadDir = UPLOAD_APATH . DS . "temp";
  		$lsUploadUrl = UPLOAD_RPATH . "/temp/";
  		
  		$laValidExtensions = array('pdf','doc','docx','odt','xls','xlsx','ods','ppt','pptx','odp','txt','csv','xml','jpg','png','gif','mp3');
  		
  		if ($psValidExtensions != null)
  		{
  			$laValidExtensions = explode(",", $psValidExtensions);
  		}	
  	
  		$this->uploader($psVarName);
  		
  		if ($psNewFileName == null)
  		{
  			$lsFile = $this->getFileName();
  			$lsFile .= microtime();
  			$this->sNewFileName = md5($lsFile) . "." .$this->getExtension();
  		}
  		else 
  		{
  			$this->sNewFileName = $psNewFileName;
  		}
  	
  		$lbResult = $this->handleUpload($lsUploadDir, $laValidExtensions);
  	
  		if (!$lbResult)
  		{
  			echo json_encode(array('success' => false, 'msg' => $this->getErrorMsg()));
  		}
  		else
  		{
  			echo json_encode(array('success' => true, 'url' => $lsUploadUrl . $this->getFileName(), 'title' => $this->getTitle(), 'file' => $this->getFileName(), 'ext' => $this->getExtension()));
  		}
  	}
  	 
  	
  	/**
  	 * 
  	 * @param string $psFile
  	 * @param string $psBaseDir
  	 * @return bool
  	 */
  	public function moveFromTemp ($psFile, $psBaseDir)
  	{
  		$lsOrigin = UPLOAD_APATH . DS . 'temp' . DS . $psFile;
  		
  		if (file_exists($lsOrigin))
  		{
  			$lsSavePath = System::createBalancedDir($psBaseDir, $psFile);
  			$lsSavePath .= DS . $psFile;
  		
  			if (file_put_contents($lsSavePath, fopen($lsOrigin, 'r')))
  			{
  				return true;
  			}
  		}
  		
  		return false;
  	}
  	
  	
  	/**
  	 * 
  	 * @param string $psFile
  	 * @return bool
  	 */
  	public function removeFromTemp ($psFile)
  	{
  		return $this->remove(UPLOAD_APATH . DS . 'temp' . DS . $psFile);	
  	}
  	
  	
  	/**
  	 * 
  	 * @param string $psFile
  	 * @return bool
  	 */
  	public function remove ($psFile)
  	{
  		if (file_exists($psFile))
  		{
  			return System::removeFile($psFile);
  		}
  		
  		return false;
  	}
  	
  	
  	/**
  	 * 
  	 */
  	public function progress ()
  	{
  		if (isset($_REQUEST['progresskey']))
  		{
  			$laStatus = apc_fetch('upload_'.$_REQUEST['progresskey']);
  		}
  		else
  		{
  			exit(json_encode(array('success' => false)));
  		}
  	
  		$lnPct = 0;
  		$lnSize = 0;
  	
  		if (is_array($laStatus))
  		{
  			if (array_key_exists('total', $laStatus) && array_key_exists('current', $laStatus))
  			{
  				if ($laStatus['total'] > 0)
  				{
  					$lnPct = round(($laStatus['current'] / $laStatus['total']) * 100);
  					$lnSize = round($laStatus['total'] / 1024);
  				}
  			}
  		}
  	
  		echo json_encode(array('success' => true, 'pct' => $lnPct, 'size' => $lnSize));
  	}
}