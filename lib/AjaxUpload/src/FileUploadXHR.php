<?php
/**
 * Simple Ajax Uploader
 * Version 1.8.2
 * https://github.com/LPology/Simple-Ajax-Uploader
 *
 * Copyright 2013 LPology, LLC
 * Released under the MIT license
 *
 * View the documentation for an example of how to use this class.
 */

namespace AjaxUpload;

/**
* Handles XHR uploads
* Used by FileUpload below -- don't call this class directly.
*/
final class FileUploadXHR 
{
    /**
     * 
     * @var String
     */
	public $sUploadName;
	
	
	/**
	 * 
	 * @param string $psSavePath
	 * @return bool
	 */
  	final public function Save($psSavePath)
  	{
    	if (false !== file_put_contents($psSavePath, fopen('php://input', 'r'))) 
    	{
      		return true;
    	}
    
	    return false;
  	}
  
  	
  	/**
  	 * 
  	 * @return string
  	 */
  	final public function getFileName() 
  	{
    	return $_GET[$this->sUploadName];
  	}
  
  	
  	/**
  	 * 
  	 * @return int
  	 * @throws Exception
  	 */
  	final public function getFileSize() 
  	{
    	if (isset($_SERVER['CONTENT_LENGTH'])) 
    	{
      		return (int)$_SERVER['CONTENT_LENGTH'];
    	}
    	else
    	{
      		throw new Exception('Content length not supported.');
    	}
  	}
}